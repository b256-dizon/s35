

const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://admin:admin1234@b256-dizon.choclmo.mongodb.net/B256_to-do?retryWrites=true&w=majority",

									{
										useNewUrlParser:true,
										useUnifiedTopology:true
								}

				);

const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
  }
});

const User = mongoose.model('User', UserSchema);


// 